Decouplage

% Cr�ation des fonctions de transfert

% Plaque
[b,a] = ss2tf(A_px,B_px,C_px,D_px);
hpx = tf(b,a)
zhpx = roots(b);
phpx = roots(a);

[b,a] = ss2tf(A_py,B_py,C_py,D_py);
hpy = tf(b,a)
zhpy = roots(b);
phpy = roots(a);

[b,a] = ss2tf(A_pz,B_pz,C_pz,D_pz);
hpz = tf(b,a)
zhpz = roots(b);
phpz = roots(a);

% Sph�re
[b,a] = ss2tf(A_sx,B_sx,C_sx,D_sx);
hsx = tf(b(1,:),a)
zhsx = roots(b(1,:));
phsx = roots(a);

[b,a] = ss2tf(A_sy,B_sy,C_sy,D_sy);
hsy = tf(b(1,:),a)
zhsy = roots(b(1,:));
phsy = roots(a);

% Analyse rlocus

figure('name','Plaque XY')
rlocus(hpx)

figure('name','Plaque Z')
rlocus(hpz)

figure('name','Sphere XY')
rlocus(-1*hsx)

% Analyse p�les-z�ros

disp('===== P�les et z�ros des fonctions de transfert =====')
table(zhpx)
table(phpx)

table(zhpz)
table(phpz)

table(zhsx)
table(phsx)