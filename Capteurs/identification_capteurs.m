% Projet S4 P2
% Identification du mod�le des capteurs

clear variables
close all
clc

load('capteur.mat')

figure(1)
hold on
plot(distance,voltage)
title('Donn�es des capteurs')
xlabel('Distance (m)')
ylabel('Tension (V)')

% Mod�le exponentiel

clear g
clear A
clear phi
clear psi

g(:,1) = exp(400*distance);
g(:,2) = exp(-400*distance);
g(:,3) = exp(10*distance);
g(:,4) = exp(-10*distance);
g(:,5) = exp(100*distance);
g(:,6) = exp(-100*distance);
g(:,7) = exp(200*distance);
g(:,8) = exp(-200*distance);

for i = 1:8
   for j = 1:8
       phi(i,j) = sum(g(:,i).*g(:,j));
   end
   
   psi(i,1) = sum(g(:,i).*voltage);
end

A = pinv(phi)*psi;
table(A)

voltage_a = 0;
for i = 1:8
   voltage_a = voltage_a + g(:,i)*A(i); 
end

plot(distance,voltage_a)

E = sum((voltage_a-voltage).^2);
RMS(1) = sqrt(E/length(distance));

yb = (1/length(distance))*sum(voltage);
R(1) = sum((voltage_a-yb).^2)/sum((voltage-yb).^2);


% Mod�le trigonom�trique

clear g
clear A
clear phi
clear psi

g(:,1) = sin(distance);
g(:,2) = cos(distance);
g(:,3) = sin(10*distance);
g(:,4) = cos(10*distance);
g(:,5) = sin(100*distance);
g(:,6) = cos(100*distance);
g(:,7) = sin(200*distance);
g(:,8) = cos(200*distance);

for i = 1:8
   for j = 1:8
       phi(i,j) = sum(g(:,i).*g(:,j));
   end
   
   psi(i,1) = sum(g(:,i).*voltage);
end

A = pinv(phi)*psi;

voltage_a = 0;
for i = 1:8
   voltage_a = voltage_a + g(:,i)*A(i); 
end

plot(distance,voltage_a)

E = sum((voltage_a-voltage).^2);
RMS(2) = sqrt(E/length(distance));

yb = (1/length(distance))*sum(voltage);
R(2) = sum((voltage_a-yb).^2)/sum((voltage-yb).^2);

% Mod�le logaritmique

clear g
clear A
clear phi
clear psi

g(:,1) = sin(100*distance);
g(:,2) = cos(100*distance);
g(:,3) = sin(200*distance);
g(:,4) = cos(200*distance);
g(:,5) = exp(100*distance);
g(:,6) = exp(-100*distance);
g(:,7) = exp(200*distance);
g(:,8) = exp(-200*distance);

for i = 1:8
   for j = 1:8
       phi(i,j) = sum(g(:,i).*g(:,j));
   end
   
   psi(i,1) = sum(g(:,i).*voltage);
end

A = pinv(phi)*psi;

voltage_a = 0;
for i = 1:8
   voltage_a = voltage_a + g(:,i)*A(i); 
end

plot(distance,voltage_a)

E = sum((voltage_a-voltage).^2);
RMS(3) = sqrt(E/length(distance));

yb = (1/length(distance))*sum(voltage);
R(3) = sum((voltage_a-yb).^2)/sum((voltage-yb).^2);

table(RMS)
table(R)