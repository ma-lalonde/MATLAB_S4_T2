% Conception du compensateur des axes de la plaque

FT_et_stabilite

%% D�but de la conception

close all
clc

g = hpx;
[numg,deng] = tfdata(g,'v');

% Crit�res de desing

mp_des = 5.0;
ts_des = 0.030;
tp_des = 0.025;
tr_des = 0.020;
erp_des = 0.0;

ajout_de_phase = 1.5;

% Calcul des p�les d�sir�s

phi = atand(-pi()/log(mp_des/100));
zeta = cosd(phi);

wn_des(1) = (1 + 1.1*zeta + 1.4*zeta^2)/tr_des;
wn_des(2) = pi()/(tp_des*sqrt(1-zeta^2));
wn_des(3) = 4/(zeta*ts_des);
wn = max(wn_des);

wa = wn*sqrt(1-zeta^2);

p_des(1) = -zeta*wn + wa*1j;
p_des(2) = -zeta*wn - wa*1j;

figure
hold on
rlocus(g)
plot(real(p_des),imag(p_des),'p')

% Calcul du compensateur AvPh

% Phase � ajouter
evalg = polyval(numg,p_des(1))/polyval(deng,p_des(1));
angleg = rad2deg(angle(evalg)) - 360;

delta_phi = (-180 - angleg + ajout_de_phase)/2;

% Calcul des p�les - z�ros

alpha = 180 - phi;

phiz  = (alpha + delta_phi)/2;
phip  = (alpha - delta_phi)/2;

z = real(p_des(1)) - imag(p_des(1))/tand(phiz);
p = real(p_des(1)) - imag(p_des(1))/tand(phip);

% Calcul du gain

numav = conv([1 -z],[1 -z]);
denav = conv([1 -p],[1 -p]);

evalav = polyval(numav,p_des(1))/polyval(denav,p_des(1));

%kav = 1/abs(evalg*evalav);
kav = 1;
gav = kav*tf(numav,denav);

% Ajout d'un PI pour annuler l'erreur en RP

z = real(p_des(1))/10;
gpi = tf([1 -z],[1 0]);

evalpi = polyval([1 -z],p_des(1))/polyval([1 0],p_des(1));

k = 1/abs(evalpi*evalav*evalg);

% Compensateur et validation

disp('Compensateur')
h_comp_angle_plaque = k*gav*gpi

rlocus(g*gav)
rlocus(g*h_comp_angle_plaque)
p = rlocus(g*h_comp_angle_plaque,1);
plot(real(p),imag(p),'s')

axis([-1000 200 -300 300])

figure
bode(g*h_comp_angle_plaque)

figure
nyquist(g*h_comp_angle_plaque)

figure
step(feedback(g*h_comp_angle_plaque,1))

% Test discret
testdiscret(h_comp_angle_plaque)