close all
clear variables
clc

sig = 1;
EQ_lineaires

disp('====== Mod�le des actionneurs ======')
disp(' ')
disp('Param�tres identifi�s:')

aE = [aE3 aE2 aE1 aE0]';
aS = [aS3 aS2 aS1 aS0]';

table(aE)
table(aS)

% Ajustement des donn�es exp�rimentales

z_m1A_adj = z_m1A(1:length(z_m2A));
Fe_m1A_adj = Fe_m1A(1:length(z_m2A));

z_m2A_adj = z_m2A;
Fe_m2A_adj = Fe_m2A;

z_pos_adj = z_pos(1:length(z_m2A));
Fs_adj = Fs(1:length(z_m2A));

Fz_exp_1A = Fe_m1A_adj + Fs_adj;
Fz_exp_2A = Fe_m2A_adj + Fs_adj;

% Calcul des donn�es non-lin�aires

Fs_nlin = -1./(aS0*ones(size(z_pos_adj))+aS1*z_pos_adj+aS2*z_pos_adj.^2+aS3*z_pos_adj.^3);

aFe_nlin = (aE0*ones(size(z_pos_adj))+aE1*z_pos_adj+aE2*z_pos_adj.^2+aE3*z_pos_adj.^3);
bFe_nlin1A = -(1+bE1);
bFe_nlin2A = -(2+2*bE1);

Fe_nlin1A = bFe_nlin1A./aFe_nlin;
Fe_nlin2A = bFe_nlin2A./aFe_nlin;

Fz_nlin_1A = Fe_nlin1A + Fs_nlin;
Fz_nlin_2A = Fe_nlin2A + Fs_nlin;

% Calcul des donn�es lin�aires

disp(' ')
disp('Mod�le lin�aire:')
disp(' ')

disp('deltaFk = dFkdZk*deltaZk + dFkdIk*deltaIk')
disp(['dFkdZk = ' num2str(double(dFKdZ))])
disp(['dFkdIk = ' num2str(double(dFKdIK))])

di1A = -IK_eq + (-1);
di2A = -IK_eq + (-2);

dz = -Z_eq + z_pos_adj;

dfz1A = dFKdZ.*dz + dFKdIK*di1A;
dfz2A = dFKdZ.*dz + dFKdIK*di2A;

Fz_lin_1A = dfz1A + FK_eq;
Fz_lin_2A = dfz2A + FK_eq;

% Graphique

figure('name','R�ponse des actionneurs')
hold on

plot(z_pos_adj,Fz_exp_1A,'linewidth',1.2)
plot(z_pos_adj,Fz_exp_2A,'linewidth',1.2)

plot(z_pos_adj,Fz_nlin_1A,'linewidth',1.2)
plot(z_pos_adj,Fz_nlin_2A,'linewidth',1.2)

plot(z_pos_adj,Fz_lin_1A,'linewidth',1.2)
plot(z_pos_adj,Fz_lin_2A,'linewidth',1.2)

hold off
legend('show')
legend('1A exp','2A exp','1A nlin','2A nlin','1A lin','2A lin')
%axis([0.003 z_pos_adj(end) -20 5])

title('Force des actionneurs')
xlabel('zk (m)')
ylabel('Fk (N)')

grid on

% Calcul d'erreur

disp(' ')
disp('Erreurs d''approximation:')
disp(' ')

z_pos_trim = z_pos_adj(z_pos_adj>0.003);

Fz_nlin_1A_trim = Fz_nlin_1A(z_pos_adj>0.003);
Fz_nlin_2A_trim = Fz_nlin_2A(z_pos_adj>0.003);

Fz_exp_1A_trim = Fz_exp_1A(z_pos_adj>0.003);
Fz_exp_2A_trim = Fz_exp_2A(z_pos_adj>0.003);

E1A = sum((Fz_nlin_1A_trim-Fz_exp_1A_trim).^2);
E2A = sum((Fz_nlin_2A_trim-Fz_exp_2A_trim).^2);

N = length(z_pos_trim);

yb1A = (1/N)*sum(Fz_exp_1A_trim);
yb2A = (1/N)*sum(Fz_exp_2A_trim);

R21A = sum((Fz_nlin_1A_trim-yb1A).^2)/sum((Fz_exp_1A_trim-yb1A).^2);
R22A = sum((Fz_nlin_2A_trim-yb2A).^2)/sum((Fz_exp_2A_trim-yb2A).^2);

RMS1A = sqrt(E1A/N);
RMS2A = sqrt(E2A/N);

disp(['L''erreur R2 � 1A est ' num2str(double(R21A))])
disp(['L''erreur R2 � 2A est ' num2str(double(R22A))])

disp(['L''erreur RMS � 1A est ' num2str(double(RMS1A))])
disp(['L''erreur RMS � 2A est ' num2str(double(RMS2A))])

% Graphique d'erreur

figure('name','�cart du mod�le lin�aire')

hold on
plot(z_pos_adj,Fz_lin_1A-Fz_nlin_1A,'linewidth',1.2)
plot(z_pos_adj,Fz_lin_2A-Fz_nlin_2A,'linewidth',1.2)
hold off

axis([0.003 z_pos_adj(end) -5 10])
legend('show')
legend('1A','2A')

title('�cart entre les mod�les lin�aires et non-lin�aires')
xlabel('Zk (m)')
ylabel('�cart (N)')
refline(0,0)
grid on