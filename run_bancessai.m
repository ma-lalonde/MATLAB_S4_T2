close all
clear all
clc
addpath('Actionneurs');
addpath('Banc_essai');
addpath('Modeles');

% Position � l'�quilibre de la sph�re (pour tests statiques)
sig = 1.0;         % Pr�sence (1) ou non (0) de la sph�re
xSeq = 0.000;      % Position x de la sph�re � l'�quilibre en metres
ySeq = 0.000;      % Position y de la sph�re � l'�quilibre en metres

%Point d'op�ration choisi pour la plaque
Axeq = 0;               %en degres
Ayeq = 0;               %en degres
Pzeq = .015;            %en metres

%Exemple de trajectoire
%t_des     = [0:1:8]'*5;
%x_des     = [t_des, [0 0 0.5 1  0 -1 0 1 0]'*0.05];
%y_des     = [t_des, [0 0 0 0 -1  0 1 0 0]'*0.05];
%z_des     = [t_des, [1 1 1 1  1  1 1 1 1]'*.015];
%ax_des     = [t_des, [0 0 0.5 1  0 -1 0 1 0]'*0.05];
%ax_des     = [t_des, [1 1 1 1  1  1 1 1 1]'*.010];
%ay_des     = [t_des, [0 0 0 0 -1  0 1 0 0]'*0.05];
%ay_des     = [t_des, [1 1 1 1  1  1 1 1 1]'*-0.005];
%tfin = 50;

%% Tajectoire de validation
load('trajectoire.mat')

[Pi, Ltr, E, Vr, TrajAB, ttAB, deplacement] = Interpolation(NAB,vAB,Ts);
[Pi, Ltr, E, Vr, TrajBA, ttBA, deplacement] = Interpolation(NBA,vBA,Ts);

tfin = ttAB+ttBA+Ts;

t_des = [0:Ts:tfin]';
x_des = [t_des,vertcat(TrajAB(:,1),TrajBA(:,1))];
y_des = [t_des,vertcat(TrajAB(:,2),TrajBA(:,2))];
z_des = [t_des, ones(size(t_des))*.015];
ax_des = [t_des, zeros(size(t_des))*.015];
ay_des = [t_des, zeros(size(t_des))*.015];

%% bancessai_ini  %faites tous vos calculs de modele ici
Decouplage

%Calcul des compensateurs
%iniCTL_ver4    %Calculez vos compensateurs ici
compensateur_angles_plaque
compensateur_hauteur
Kp_Kv

close all
clc

%% simulation
warning off
open_system('DYNctl_ver4_etud_obfusc')
set_param('DYNctl_ver4_etud_obfusc','AlgebraicLoopSolver','LineSearch')
sim('DYNctl_ver4_etud_obfusc')
warning on
%% affichage

%trajectoires
figure(1)
subplot(2,3,1)
hold on;
plot(tsim, x_des_out)
plot(tsim, Px_mes_out)
title('Px')
hold off

subplot(2,3,2)
hold on
plot(tsim, y_des_out)
plot(tsim, Py_mes_out)
title('Py')
hold off

subplot(2,3,3)
plot(tsim, limits_warning)
title('Limits Warnings')
legend('show')
legend('Ax','Ay','Z')

subplot(2,3,4)
plot(tsim, zD_mes_out)
title('zD_m_e_s')

subplot(2,3,5)
plot(tsim, zE_mes_out)
title('zE_m_e_s')

subplot(2,3,6)
plot(tsim, zF_mes_out)
title('zF_m_e_s')

figure(2)
hold on
plot(x_des(:,2),y_des(:,2))
plot(x_des(1,2),y_des(1,2),'s')
plot(Px_mes_out,Py_mes_out)
plot(Px_mes_out(1),Py_mes_out(1),'o')
plot(Px_mes_out(end),Py_mes_out(end),'p')
hold off
title('Tajectoire de la bille')
xlabel('Px')
ylabel('Py')

%% Index de performance

disp('======= Performance lors de l''essai =======')

err = sqrt((x_des_out-Px_mes_out).^2+(y_des_out-Py_mes_out).^2);
err2 = err.^2;

h = tsim(2)-tsim(1);

F = (h/2)*(err2(1)+err2(end)+2*sum(err2(2:end-1)));

dFa = (err2(2)-err2(1))/h;
dFb = (err2(end)-err2(end-1))/h;

I = sqrt(F);
E = (h^2/h)*(dFb-dFa);

disp(['Index de performance : ' num2str(I)])
disp(['Erreur : ' num2str(E)])