function [Pi, Ltr, E, Vr, Traj, tt, deplacement] = InterpolationCirculaire(xy, v, ts)
M = 101;
% Fermeture de la trajectoire
xy = [xy;xy(1,:)];

% Creation d'une echelle arbitraire de temps
t = 0:1:length(xy)-1;

% Calcul des splines sur l'echelle pour x et y
px = csape(t,xy(:,1),'periodic');
py = csape(t,xy(:,2),'periodic');
Pi = [px py];

% Calcul des deplacements totaux pour x et y

tq = 0:t(end)/(M-1):t(end);
pxval = ppval(px,tq);
pyval = ppval(py,tq);

dx = diff(pxval);
dy = diff(pyval);

dist = [0 sqrt(dx.^2 + dy.^2)];
for i = 2:length(dist)
    dist(i) = dist(i) + dist(i-1);
end
Ltr = [(1:1:M)' dist'];
h = tq(2) - tq(1);
E = h^2/12 * ((dist(end)-dist(end-1))/h - (dist(2) - dist(1))/h);

distot = dist(end) - E;

% Calcul temps echantillonnage
tt = distot/v;
O = round(tt/ts)+1;
tt = (O-1)*ts;
Vr = distot/tt;

tol = 1e-08;
Traj = zeros(O,2);
Traj(1,:) = xy(1,:);
distCumul = 0;
tdprecedent = 0;
for i = 2:O
    distdes = distot / (O-1)*(i-1);
    td = distdes / distot * t(end);
    difference = tol+1;
    j = 0;
    while difference > tol && j < 30
        ddist = sqrt((ppval(px,td) - ppval(px,td-(1e-08)))^2 + (ppval(py,td) - ppval(py,td-(1e-08)))^2)/1e-08;
        distSegment = sqrt((ppval(px,td) - Traj(i-1,1))^2 + (ppval(py,td) -Traj(i-1,2))^2);
        
        td = td - (distCumul + distSegment - distdes)/ddist;
        td = max(td,tdprecedent);
        difference = abs(distCumul + distSegment - distdes);
        j = j + 1;
    end
    tdprecedent = td;
    distCumul = distCumul + distSegment;
    Traj(i,1) = ppval(px,td);
    Traj(i,2) = ppval(py,td);
end
% Traj(end,:) = xy(end,:);

tdeplacement = ts*(0:1:O-1)';
deplacement = [tdeplacement Traj];

end