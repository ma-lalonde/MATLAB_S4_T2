clc
% le code est valable pour la position en y comme en x.

% x'' = Kp(xd - x) + Kv(xd' - x')
% Xs^2 = Kp(Xd - X) + Kv(Xds - Xs)
% Xs^2 = KpXd - KpX + KvXds - KvXs
% Xs^2 + KvXs + KpX = KpXd + KvXds
% X(s^2 + Kvs + Kp) = Xd(Kp + Kvs)

% X   =     Kv s + Kp
% Xd      s^2 + Kv s + Kp

zeta = 0.9;
ts   = 3;
wn   = 1.48;

[b,a] = tfdata(hsx,'v');

Kp = (wn^2)/abs(b(end));      %------------------------------------------------------------>  2.1904
Kv = (2*zeta*wn)/abs(b(end)); %------------------------------------------------------------>  2.6640

num = [Kv Kp];
den = [1 Kv Kp];
f = tf(num,den); % fonction de transfert

t =[0:0.01:10];
u = ones(size(t));
y = lsim(f,u,t);

figure %---------------------------------------------------------------------> stablilisation � 3.535 s
plot(t,y)
axis([0 10 0 1.4]) 
erreurhaut = 1.02*ones(size(t));
erreurbas = 0.98*ones(size(t));
ploterreurhaut = line(t,erreurhaut,'linestyle','--','color','r');
ploterreurbas = line(t,erreurbas,'linestyle','--','color','r');
hold off


BW = wn*sqrt((1-2*zeta^2)+ sqrt(4*zeta^4 - 4*zeta^2 +2)); % -----------------> BW = 1.1042e+000 rad/s
