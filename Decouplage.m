EQ_lineaires


% D�couplage

%Plaque
A_p = [zeros(3) eye(3) zeros(3);PP_ zeros(3) PC_; zeros(3) zeros(3) CC_];
B_p = [zeros(3);zeros(3);CV_];
C_p = [TDEF' zeros(3) zeros(3)];
D_p = zeros(3);
deltax_p = [deltax_(1:6); deltax_(11:13)];

% Changement de coordonnees
deltax_phithetaz =[deltax_p(1:6); (U * deltax_p(7:9))]; % vers i phi-theta-z
deltau_phithetaz = U * u_ABC;

%% D�couplage de la plaque

PC_dec = PC_ / TABC;

A_pdec = A_p;
A_pdec(4:6,7:9) = PC_dec;
C_pdec = [eye(3) zeros(3) zeros(3)];

%% S�paration des axes

% Systeme plaque phi
A_px = [A_pdec(1,1) A_pdec(1, 4) A_pdec(1,7); A_pdec(4,1) A_pdec(4, 4) A_pdec(4,7); A_pdec(7,1) A_pdec(7,4) A_pdec(7,7)];
B_px = [B_p(1,1); B_p(4,1); B_p(7,1)];
C_px = [1 0 0];
D_px = 0;
deltau_px = deltau_phithetaz(1);
deltax_px = [deltax_phithetaz(1); deltax_phithetaz(4); deltax_phithetaz(7)];

% Systeme plaque theta
A_py = [A_pdec(2,2) A_pdec(2, 5) A_pdec(2,8); A_pdec(5,2) A_pdec(5, 5) A_pdec(5,8); A_pdec(8,2) A_pdec(8,5) A_pdec(8,8)];
B_py = [B_p(2,2); B_p(5,2); B_p(8,2)];
C_py = [1 0 0];
D_py = 0;
deltau_py = deltau_phithetaz(2);
deltax_py = [deltax_phithetaz(2); deltax_phithetaz(5); deltax_phithetaz(8)];

% Systeme plaque phi
A_pz = [A_pdec(3,3) A_pdec(3, 6) A_pdec(3,9); A_pdec(6,3) A_pdec(6, 6) A_pdec(6,9); A_pdec(9,3) A_pdec(9,6) A_pdec(9,9)];
B_pz = [B_p(3,3); B_p(6,3); B_p(9,3)];
C_pz = [1 0 0];
D_pz = 0;
deltau_pz = deltau_phithetaz(3);
deltax_pz = [deltax_phithetaz(3); deltax_phithetaz(6); deltax_phithetaz(9)];

%% S�paration des axes de la sph�re

% Sphere
A_s = [zeros(2) eye(2); zeros(2,4)];
B_s = [zeros(2,3); SP_];
C_s = eye(4);
D_s = zeros(4,3);
deltax_s = deltax_(7:10);
deltau_p = deltax_p(1:3);

A_sx = [0 1; 0 0];
B_sx = [0; B_s(3, 2)];
C_sx = eye(2);
D_sx = zeros(2,1);
deltax_sx = [deltax_s(1,:) deltax_s(3,:)];

A_sy = [0 1;0 0];
B_sy = [0; B_s(4,1)];
C_sy = eye(2);
D_sy = zeros(2,1);
deltax_sy = [deltax_s(2,:) deltax_s(4,:)];