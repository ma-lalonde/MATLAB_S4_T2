function [Pi, Ltr, E, Vr, Traj, tt, deplacement] = Interpolation(xy, v, ts)
M = 101;
inverser = 0;
if xy(1,1) > xy(end,1)
    xy = [flipud(xy(:,1)) flipud(xy(:,2))];
    inverser = 1;
end

Pi = polyfit(xy(:,1),xy(:,2), length(xy(:,1)-1));
df = polyder(Pi);
rangex = xy(1,1):(xy(end,1)-xy(1,1))/(M-1):xy(end,1);
g = sqrt(1 + polyval(df,rangex).^2);

h = (rangex(2)-rangex(1));
L = h/2 * (g(1) + g(end) + 2*sum(g(2:end-1)));
E = h^2/12*((g(end)-g(end-1))/h - (g(2)-g(1))/h);
L = L - E;

Lcalc = [0 (h/2*(g(2:end) + g(1:end-1)))];
for i = 2:length(Lcalc)
Lcalc(i) = Lcalc(i) + Lcalc(i-1);
end

Ltr = [(1:1:M)' Lcalc'] ;



% Calcul temps echantillonnage
n = 3;
tt = L/v;
O = round(tt/ts)*n+1;
tt = (O-1)/n*ts;
Vr = L/tt;


tol = 1e-08;
Traj = zeros(O,2);
Traj(1,:) = xy(1,:);
distParcourue = 0;
for i = 2:O
    distdes = (L-distParcourue) / (O - i + 1);
    xdes = Traj(i-1,1) + xy(end,1)-Traj(i-1,1)/(O - i + 1);
    difference = tol+1;
    
    j = 0;
    while difference > tol && j < 30
        xdes = xdes - ((sqrt(1 + polyval(df,xdes)^2) + sqrt(1 + polyval(df,Traj(i-1,1))^2))*(xdes-Traj(i-1,1))/2 - distdes)/sqrt(1 + polyval(df,xdes)^2);
        xdes = max(Traj(i-1,1),xdes);
        difference = abs(sqrt((xdes - Traj(i-1,1))^2 + (polyval(Pi,xdes)-Traj(i-1,2))^2) - distdes);
        j = j + 1;
    end
    distParcourue = distParcourue + sqrt((xdes - Traj(i-1,1))^2 + (polyval(Pi,xdes)-Traj(i-1,2))^2);
    Traj(i,1) = xdes;
    Traj(i,2) = polyval(Pi,xdes);
end
% Traj(end,:) = xy(end,:);
Traj = Traj(1:n:end,:);



if inverser == 1
    Traj = [flipud(Traj(:,1)) flipud(Traj(:,2))];
end

tdeplacement = ts*(0:1:(O-1)/n)';
deplacement = [tdeplacement Traj];

end